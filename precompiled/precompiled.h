#pragma once

#define GLM_FORCE_RADIANS
#define SDL_MAIN_HANDLED
#define _SCL_SECURE_NO_WARNINGS // Boost gives a warning, which is considered an error

#include <cassert>
#include <vector>
#include <iostream>
#include <stdexcept>
#include <string>
#include <SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <boost/signals2/signal.hpp>
#include <boost/bind/bind.hpp>
#include <lodepng.h>

#include "Constants.h"
#include "Typedefs.h"