#pragma once
#include "Typedefs.h"

const uint32_t WINDOW_WIDTH = 1920;
const uint32_t WINDOW_HEIGHT = 1080;
const std::string programName = "Vulkan Renderer";
const uint32_t sdlWindowFlags = SDL_WINDOW_RESIZABLE;