#include "Input.h"
#include <SDL.h>
#include <boost/signals2/signal.hpp>
#include <boost/bind/bind.hpp>

class InputHandler::Impl
{
public:
	bool init()
	{
		return true;
	}

	void shutDown()
	{
		// nop
	}

	void pollEvents()
	{
		SDL_Event sdlEvent;

		if (SDL_PollEvent(&sdlEvent))
		{
			InputEvent e(sdlEvent);
			if (e.getDetails().type == SDL_QUIT || e.getDetails().type == SDL_KEYDOWN && e.getDetails().key.keysym.sym == SDLK_ESCAPE)
			{
				programShouldBeClosed = true;
				e.use();
			}

			// Signal others
			inputEventSignal(e);
		}
	}

	bool getProgramShouldBeClosed() { return programShouldBeClosed; }
	void setProgramShouldBeClosed(const bool close) { programShouldBeClosed = close; }

	InputSignal inputEventSignal;

private:
	bool programShouldBeClosed = false;
};

InputHandler::InputHandler()
{
}

InputHandler::~InputHandler()
{
	shutDown();
}

bool InputHandler::init()
{
	if (!impl)
		impl = std::make_unique<Impl>();
	return impl->init();
}

void InputHandler::shutDown()
{
	if (impl)
		impl->shutDown();
	impl.reset(nullptr);
}

void InputHandler::pollEvents()
{
	if (impl)
		impl->pollEvents();
}

bool InputHandler::getProgramShouldBeClosed()
{
	if (impl)
		return impl->getProgramShouldBeClosed();
	return false;
}

void InputHandler::setProgramShouldBeClosed(const bool close)
{
	if (impl)
		impl->setProgramShouldBeClosed(close);
}

boost::signals2::connection InputHandler::addInputHandler(const InputSignal::slot_type& slot) const
{
	if (impl)
		return impl->inputEventSignal.connect(slot);

	return boost::signals2::connection();
}
