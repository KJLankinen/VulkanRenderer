#pragma once

class InputEvent
{
public:
	InputEvent() {}
	InputEvent(SDL_Event e)
		: e(e)
	{}
	~InputEvent() {}

	void use() { used = true; }
	bool isUsed() { return used; }
	SDL_Event getDetails() { return e; }

private:
	SDL_Event e;
	bool used = false;
};