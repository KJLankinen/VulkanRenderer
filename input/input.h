#pragma once
#include <memory>
#include <functional>
#include <boost\signals2\connection.hpp>
#include "InputEvent.h"

class InputHandler
{
public:
	InputHandler();
	~InputHandler();
	bool init();
	void shutDown();
	void pollEvents();
	bool getProgramShouldBeClosed();
	void setProgramShouldBeClosed(const bool close);

	typedef boost::signals2::signal<void(InputEvent)> InputSignal;
	boost::signals2::connection addInputHandler(const InputSignal::slot_type& slot) const;
private:
	class Impl;
	std::unique_ptr<Impl> impl;
};