#include <chrono>
#include "Renderer.h"
#include "Input.h"

template <typename T>
struct SubSystemWrapper
{
	T handle;
	bool initSuccess = false;
};

SubSystemWrapper<Renderer> gRenderer;
SubSystemWrapper<InputHandler> gInputHandler;

int main()
{
	bool success = true;
	SDL_Window* window = nullptr;
	double deltaTime = 0.0;
	std::chrono::high_resolution_clock::time_point t_now, t_last;
	t_now = std::chrono::high_resolution_clock::now();

	// -------------------
	// Initialization start
	// --------------------
	{
		// SDL
		// TODO: Add SDL_GetError() checks.
		if (SDL_Init(SDL_INIT_VIDEO) < 0)
			success = false;
		window = SDL_CreateWindow(programName.c_str(), 100, 100, WINDOW_WIDTH, WINDOW_HEIGHT, sdlWindowFlags);
		if (!window)
			success = false;
		else
		{
			try
			{
				gInputHandler.initSuccess = gInputHandler.handle.init();
				gRenderer.initSuccess = gRenderer.handle.init(window, &gInputHandler.handle);
			}
			catch (const std::runtime_error& e)
			{
				std::cerr << e.what() << std::endl;
				success = false;
			}
		}
		success &=
			gInputHandler.initSuccess &&
			gRenderer.initSuccess;
	}
	// ------------------
	// Initialization end
	// ------------------


	// ---------------
	// Main loop start
	// ---------------
	if (success)
	{
		while (!gInputHandler.handle.getProgramShouldBeClosed())
		{
			try
			{
				t_now = std::chrono::high_resolution_clock::now();
				deltaTime = std::chrono::duration_cast<std::chrono::duration<double, std::chrono::seconds::period>>(t_now - t_last).count();
				t_last = t_now;

				// Input
				gInputHandler.handle.pollEvents();

				// Update
				gRenderer.handle.update(deltaTime);

				// Render
				gRenderer.handle.render();
			}
			catch (const std::runtime_error& e)
			{
				std::cerr << e.what() << std::endl;
				success = false;
				gInputHandler.handle.setProgramShouldBeClosed(true);
			}
		}
	}
	// -------------
	// Main loop end
	// -------------


	// ---------------
	// Shut down start
	// ---------------
	{
		if (gRenderer.initSuccess)
			gRenderer.handle.shutDown();
		if(gInputHandler.initSuccess)
			gInputHandler.handle.shutDown();

		SDL_DestroyWindow(window);
		SDL_Quit();
	}
	// -------------
	// Shut down end
	// -------------

	return success ? EXIT_SUCCESS : EXIT_FAILURE;
}