#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 vColor;
layout(location = 1) in vec2 vTexCoord;

layout(location = 0) out vec4 fColor;

layout(binding = 1) uniform sampler2D texSampler;

void main()
{
    fColor = vec4(vColor, 0.0);//texture(texSampler, vTexCoord);
}