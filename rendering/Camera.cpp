#include "Camera.h"
#include <cmath>

Camera::Camera()
	: position(glm::vec3(2.0f, 0.0f, 0.0f))
	, direction(glm::vec3(-1.0f, 0.0f, 0.0f))
	, right(glm::vec3(0.0f, 1.0f, 0.0f))
	, up(glm::vec3(0.0f, 0.0f, 1.0f))
	, near(0.1f)
	, far(10.0f)
	, fov(45.0f)
{
}

Camera::~Camera()
{
	if (inputConnection.connected())
		inputConnection.disconnect();
}

bool Camera::connectInputHandler(InputHandler* handler)
{
	if (handler && !inputConnection.connected())
		inputConnection = handler->addInputHandler(boost::bind(&Camera::handleInput, this, _1));

	return inputConnection.connected();
}

void Camera::handleInput(InputEvent& ie)
{
	if (ie.isUsed())
		return;
	
	SDL_Event e = ie.getDetails();

	bool isWindowEvent =	e.type == SDL_WINDOWEVENT;
	bool keyPressed =		e.type == SDL_KEYDOWN;
	bool keyReleased =		e.type == SDL_KEYUP;
	bool isMouseMotion =	e.type == SDL_MOUSEMOTION;
	bool isMouseWheel =		e.type == SDL_MOUSEWHEEL;
	bool isResized =		e.window.type == SDL_WINDOWEVENT_RESIZED;
	bool isUpKey =			e.key.keysym.sym == SDLK_w		|| e.key.keysym.sym == SDLK_UP;
	bool isDownKey =		e.key.keysym.sym == SDLK_s		|| e.key.keysym.sym == SDLK_DOWN;
	bool isRightKey =		e.key.keysym.sym == SDLK_d		|| e.key.keysym.sym == SDLK_RIGHT;
	bool isLeftKey =		e.key.keysym.sym == SDLK_a		|| e.key.keysym.sym == SDLK_LEFT;
	bool isShiftKey =		e.key.keysym.sym == SDLK_LSHIFT;

	if (keyPressed || keyReleased)
	{
		if (isUpKey || isDownKey || isRightKey || isLeftKey || isShiftKey)
		{
			ie.use();

			if (isUpKey)
			{
				if (keyPressed)
					velocityForward = 1.0f;
				else if (keyReleased && velocityForward > 0.0f)
					velocityForward = 0.0f;
			}
			else if (isDownKey)
			{
				if (keyPressed)
					velocityForward = -1.0f;
				else if (keyReleased && velocityForward < 0.0f)
					velocityForward = 0.0f;
			}
			else if (isRightKey)
			{
				if (keyPressed)
					velocityRight = 1.0f;
				else if (keyReleased && velocityRight > 0.0f)
					velocityRight = 0.0f;
			}
			else if (isLeftKey)
			{
				if (keyPressed)
					velocityRight = -1.0f;
				else if (keyReleased && velocityRight < 0.0f)
					velocityRight = 0.0f;
			}
			else if (isShiftKey)
			{
				if (keyPressed)
					speed = 10.0f;
				else if (keyReleased)
					speed = 1.0f;
			}
		}
	}
	else if (isWindowEvent)
	{
		if (isResized)
		{
			windowWidth = e.window.data1;
			windowHeight = e.window.data2;
		}
	}
	else if (isMouseMotion)
	{
		dxdy = glm::vec2((float)e.motion.xrel / (float)windowWidth, (float)e.motion.yrel / (float)windowHeight) * 2.0f * (float)M_PI;
	}
	else if (isMouseWheel)
	{
		if (SDL_GetRelativeMouseMode())
		{
			mouseSpeed -= 0.01f * (float)e.wheel.y;
			mouseSpeed = fmax(fmin(5.0f, mouseSpeed), 0.05f);
		}
		else
		{
			position += direction * speed * (float)e.wheel.y * 0.1f;
		}
	}
}

void Camera::update(double dt)
{
	deltaTime = (float)dt;
	updateCameraPosition();
	updateCameraDirection();
}

void Camera::updateCameraPosition()
{
	position += velocityForward * direction * deltaTime * speed
		+ velocityRight * right * deltaTime * speed;
}

void Camera::updateCameraDirection()
{
	// Only update based on mouse movement, if the cursor is not visible
	if (!SDL_GetRelativeMouseMode())
		return;

	yawPitch += dxdy * mouseSpeed;
	dxdy = glm::vec2(0.0f);
	if (yawPitch.y < -M_PI / 2.0f)
		yawPitch.y = -(float)M_PI / 2.0f + fabs(yawPitch.y + (float)M_PI / 2.0f);
	else if (yawPitch.y > M_PI / 2.0f)
		yawPitch.y = (float)M_PI / 2.0f - fabs(yawPitch.y - (float)M_PI / 2.0f);

	direction = glm::vec3(cos(yawPitch.y) * sin(yawPitch.x), cos(yawPitch.y) * cos(yawPitch.x), sin(-yawPitch.y));
	glm::vec3 tempVec = direction;
	tempVec.z *= -0.5f;
	tempVec.z += 0.5f;
	float directionMultiplier = tempVec.z < direction.z ? -1.0f : 1.0f;

	right = directionMultiplier * glm::normalize(glm::cross(direction, tempVec));
	up = glm::cross(right, direction);
}
