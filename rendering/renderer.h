#pragma once

#include <memory>
#include "Input.h"

class Renderer
{
public:
	Renderer();
	~Renderer();
	bool init(SDL_Window* window, InputHandler* handler);
	void render();
	void shutDown();
	void update(double deltaTime);
private:

	class Impl;
	std::unique_ptr<Impl> impl;
};