#pragma once

#include <chrono>
#include "Input.h"
#include "InputEvent.h"

#ifdef far
#undef far
#endif

#ifdef near
#undef near
#endif

class Camera
{
	/*
	* Convention: Right handed coordinates with +X to right, +Y down and +Z towards or into the screen.
	* This is assumed by Vulkan, and since this is meant as a Vulkan renderer, adopting the same convention here.
	*/
public:
	Camera();
	~Camera();

	bool connectInputHandler(InputHandler* handler);
	void handleInput(InputEvent& ie);
	void update(double dt);

	// Getters
	float getNear() { return near; }
	float getFar() { return far; }
	float getFov() { return fov; }
	glm::vec3 getPosition() { return position; };
	glm::vec3 getDirection() { return direction; };
	glm::vec3 getUp() { return up; };
	glm::mat4 getLookAt() { return glm::lookAt(position, position + direction, up); }

	// Setters
	void setNear(float newNear) { near = newNear; }
	void setFar(float newFar) { far = newFar; }
	void setFov(float newFov) { fov = newFov; }
	void setPosition(glm::vec3 newPosition) { newPosition = position; }
	void setDirection(glm::vec3 newDirection) { newDirection = direction; }
	void setUp(glm::vec3 newUp) { newUp = up; }

private:
	uint32_t windowWidth = WINDOW_WIDTH;
	uint32_t windowHeight = WINDOW_HEIGHT;
	float near;
	float far;
	float fov;
	float velocityForward = 0.0f;
	float velocityRight = 0.0f;
	float speed = 1.0f;
	float deltaTime = 0.0f;
	float mouseSpeed = 0.1f;

	glm::vec2 yawPitch;
	glm::vec2 dxdy;
	glm::vec3 position;
	glm::vec3 direction;
	glm::vec3 up;
	glm::vec3 right;

	boost::signals2::connection inputConnection;

	void updateCameraPosition();
	void updateCameraDirection();
};